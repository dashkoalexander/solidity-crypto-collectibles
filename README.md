## CRYPTO PLANETS

User is able to: 
1) buy the planet -> system generates planet's properties and calculates the recommended price based on rules (see below);
2) ruin the planet -> planet becomes older and population is changed randomly -> gives a chance to get a more valuable planet (encouraging the user to risk and pay real money for a potential profit);
3) create a buy request for the planet owned by another player -> user can ignore the recommended price and set it higher or lower, ETH is locked inside Sales Manager contract until request is approved or cancelled;
4) cancel the created buy request;
5) accept the buy request from another user -> ETH is transferred from the buyer to seller.

----

Rules for estimation of recommended price:
1) the bigger age => the more interesting collectible => price increases;
2) the bigger galaxyId (means that planet is far away from Earth) => the more interesting collectible => price increases;
3) the bigger population => the more interesting collectible => price increases;

With the current model of price estimation user can get a planet that worth of almost 10 ETH, though it will happen extremely rare, but user is still encouraged to risk for getting a valuable collectible.

## TESTS

JS tests done with Chai and Truffle Assertions (gives ability to catch revert(), assert(), require(), emitted events) packages.

Commands for running tests (all packages should be already fetched, truffle folder is opened in terminal):
- truffle develop
- compile
- test

----

Also tested in a private Geth node, you can set your own with a predefined "genesis.json" as follows:
1) geth --datadir ./network init genesis.json
2) geth --identity "TESTNODE" --datadir ./network --ethash.dagdir ./network/.ethash --nodiscover --port "30303" --rpc --rpcaddr "localhost" --rpcport "8545" --rpccorsdomain "*" --rpcapi "db,eth,net,web3" --ws --wsaddr "localhost" --wsport "8546" --wsorigins "*" --networkid 23112018 --nat "any" console
3) personal.newAccount("password")
4) eth.coinbase
5) personal.newAccount("password2")
6) miner.start(1) //for some time to get a bit of ETH
7) web3.miner.setEtherbase(web3.eth.accounts[1])
8) miner.start(1) //for some time to get a bit of ETH
9) close the node
10) geth --identity "TESTNODE" --datadir ./network --ethash.dagdir ./network/.ethash --nodiscover --port "30303" --rpc --rpcaddr "localhost" --rpcport "8545" --rpccorsdomain "*" --rpcapi "db,eth,net,web3" --ws --wsaddr "localhost" --wsport "8546" --wsorigins "*" --networkid 23112018 --nat "any" --unlock "0,1" --password "passwords.txt" console

Now Geth node is ready and tests can be done with simply:
- truffle test --network development

## DEPLOYMENT

Consider one of these commands:
- truffle migrate --network ropsten
- truffle migrate --network development

Contract migration is shown inside the tests, though not performed during the initial migration of contracts while executing above commands.