var HDWalletProvider = require("truffle-hdwallet-provider");
const MNEMONIC = 'oval second snack lock blade forward guard genre reason unveil home laptop';

module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 8545,
      network_id: "*",
      gas: 4700000
    },
    ropsten: {
      provider: function() {
        return new HDWalletProvider(MNEMONIC, "https://ropsten.infura.io/v3/841a7a20729947b9af5d18fc31b49007")
      },
      network_id: 3,
      gas: 6700000,
      gasPrice: 10000000000,
    },
    rinkeby: {
      provider: function() {
        return new HDWalletProvider(MNEMONIC, "https://rinkeby.infura.io/v3/841a7a20729947b9af5d18fc31b49007")
      },
      network_id: 4,
      gas: 6700000,
      gasPrice: 10000000000,
    }
  }
};