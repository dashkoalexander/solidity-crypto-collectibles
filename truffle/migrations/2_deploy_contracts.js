const CryptoPlanets = artifacts.require("CryptoPlanets");
const SalesManager = artifacts.require("SalesManager");
const PriceHelper = artifacts.require("PriceHelper");

module.exports = async deployer => {
  return deployer.deploy(PriceHelper).then(() => { 
    console.log('DEPLOYED PriceHelper at address: ', PriceHelper.address);
    return deployer.deploy(CryptoPlanets, PriceHelper.address).then((CryptoPlanetsInstance) => { 
      console.log('DEPLOYED CryptoPlanets at address: ', CryptoPlanets.address);
      return deployer.deploy(SalesManager, CryptoPlanets.address).then((SalesManagerInstance) => { 
        console.log('DEPLOYED SalesManager at address: ', SalesManager.address);
        CryptoPlanetsInstance.setSalesManagerAddress(SalesManager.address);
      });
    });
  });
};
