pragma solidity ^0.4.24;

import "./Owner.sol";
import "./interfaces/ICryptoPlanetsNew.sol";
import "./interfaces/IPriceHelper.sol";
import './interfaces/IMigrationAgent.sol';
import './libraries/SafeMath256.sol';

contract CryptoPlanetsNew is ICryptoPlanetsNew, Ownable
{
    using SafeMath256 for uint256;

    struct Planet
    {
        address tokenOwner;
        uint256 recommendedPrice;
        uint48 age;
        uint64 galaxyId;
        uint48 population;
        uint16 colorValue;
        //other implementation could be an RGB color scheme as follows
        //uint8 rValue;
        //uint8 gValue;
        //uint8 bValue;
    }

    mapping (uint256 => Planet) private tokens;
    mapping (uint256 => address) private tokenApprovals;
    mapping (address => uint256) private ownedTokensCount;

    event Transfer(address indexed from, address indexed to, uint256 indexed tokenId);
    event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId);
    event PlanetCreated(uint256 indexed tokenId, uint256 price, uint48 age, uint48 population, uint16 color, uint64 galaxyId);
    event PlanetRuined(uint256 indexed tokenId, uint256 newPrice, uint48 newAge, uint48 newPopulation);
    event Migrate(address indexed owner, uint256 indexed tokenId);

    uint256 private totalSupply;
    address public salesManagerAddress;
    IPriceHelper public priceHelperContract;
    address public migrationAgent;
    IMigrationAgent public migrationAgentContract;
    bool private isMigrated;

    constructor (address _priceHelperAddress) public
    {
        setPriceHelperContract(_priceHelperAddress);
        isMigrated = false;
        totalSupply = 0;
    }
    
    function setSalesManagerAddress(address _salesManagerAddress) public onlyOwner
    {
        salesManagerAddress = _salesManagerAddress;
    }

    function setPriceHelperContract(address _priceHelperAddress) public onlyOwner
    {
        priceHelperContract = IPriceHelper(_priceHelperAddress);
    }
    
    function balanceOf(address owner) external view returns (uint256) 
    {
        require(owner != address(0), "Invalid address.");
        return ownedTokensCount[owner];
    }

    function ownerOf(uint256 tokenId) public view returns (address) 
    {
        address owner = tokens[tokenId].tokenOwner;
        require(owner != address(0), "Invalid address.");
        return owner;
    }
    
    function approve(address to, uint256 tokenId) external 
    { 
        address owner = ownerOf(tokenId);
        require(to != owner, "Invalid address.");
        require(msg.sender == owner, "Function can be called only from account-owner of the specified token.");

        tokenApprovals[tokenId] = to;
        emit Approval(owner, to, tokenId);
    }

    function getApproved(uint256 tokenId) public view returns (address) 
    {
        require(exists(tokenId), "Specified token doesn't exist.");
        return tokenApprovals[tokenId];
    }
    
    function transferFrom(address from, address to, uint256 tokenId) external
    {
        require(isApprovedOrOwner(msg.sender, tokenId) || msg.sender == salesManagerAddress, "Calling address is not an owner and not allowed to transfer the specified token.");
        require(ownerOf(tokenId) == from, "Specified from address is not the owner of the specified token.");
        require(to != address(0), "Invalid address.");

        clearApproval(from, tokenId);
        removeTokenFrom(from, tokenId);
        addTokenTo(to, tokenId);

        emit Transfer(from, to, tokenId);
    }
    
    function exists(uint256 tokenId) internal view returns (bool) 
    {
        address owner = tokens[tokenId].tokenOwner;
        return owner != address(0);
    }

    function isApprovedOrOwner(address spender, uint256 tokenId) internal view returns (bool)
    {
        address owner = ownerOf(tokenId);
        return (spender == owner || getApproved(tokenId) == spender);
    }

    function mint(address to) external returns(uint256)
    {
        require(msg.sender == salesManagerAddress || msg.sender == owner, "Only Sales Manager or owner can create the planets.");
        require(to != address(0), "Invalid address.");

        uint256 createdTokenId = totalSupply;
        addTokenTo(to, createdTokenId);

        uint16 generatedColorValue;
        uint48 generatedAge;
        uint48 generatedPopulation;
        uint64 generatedGalaxyId;
        (generatedColorValue, generatedAge, generatedPopulation, generatedGalaxyId) = priceHelperContract.getRandomNumbers(createdTokenId);

        tokens[createdTokenId].age = generatedAge;
        tokens[createdTokenId].galaxyId = generatedGalaxyId;
        tokens[createdTokenId].colorValue = generatedColorValue;
        tokens[createdTokenId].population = generatedPopulation;
        uint256 generatedPrice = priceHelperContract.calculatePlanetPrice(generatedAge, generatedGalaxyId, generatedPopulation);
        tokens[createdTokenId].recommendedPrice = generatedPrice;

        totalSupply = totalSupply.add(1);
        emit Transfer(address(0), to, createdTokenId);
        emit PlanetCreated(createdTokenId, generatedPrice, generatedAge, generatedPopulation, generatedColorValue, generatedGalaxyId);

        return createdTokenId;
    }

    //gives a chance to increase a recommended price for token by changing it's inner state 
    function ruinPlanet(uint256 tokenId) external 
    {
        require(msg.sender == salesManagerAddress, "Only Sales Manager can ruin the planets.");

        Planet storage planet = tokens[tokenId];
        if(planet.age + 1000000 > planet.age)
        {
            planet.age = planet.age + 1000000;
        }
        planet.population = priceHelperContract.getRandomUINT48(tokenId);
        planet.recommendedPrice = priceHelperContract.calculatePlanetPrice(planet.age, planet.galaxyId, planet.population);

        emit PlanetRuined(tokenId, planet.recommendedPrice, planet.age, planet.population);
    }

    function addTokenTo(address to, uint256 tokenId) internal 
    {
        require(tokens[tokenId].tokenOwner == address(0), "Invalid address.");
        
        tokens[tokenId].tokenOwner = to;
        ownedTokensCount[to] = ownedTokensCount[to].add(1);
    }

    function removeTokenFrom(address from, uint256 tokenId) internal 
    {
        ownedTokensCount[from] = ownedTokensCount[from].sub(1);
        tokens[tokenId].tokenOwner = address(0);
    }

    function clearApproval(address owner, uint256 tokenId) internal 
    {
        require(ownerOf(tokenId) == owner);
        if (tokenApprovals[tokenId] != address(0)) 
        {
            tokenApprovals[tokenId] = address(0);
        }
    }

    function setMigrationAgent(address agent) external onlyOwner
    {
        require(migrationAgent == 0, "Migration Agent was specified already");
        require(!isMigrated, 'Contract was already migrated');
        migrationAgent = agent;
        migrationAgentContract = IMigrationAgent(agent);
    }

    function receiveMigratedData(address owner, uint48 age, uint64 galaxyId, uint48 population, uint16 colorValue, uint256 recommendedPrice) external
    {
        require(migrationAgent != address(0), "Migration agent is not specified yet");

        uint256 createdTokenId = totalSupply;
        addTokenTo(owner, createdTokenId);

        tokens[createdTokenId].age = age;
        tokens[createdTokenId].galaxyId = galaxyId;
        tokens[createdTokenId].colorValue = colorValue;
        tokens[createdTokenId].population = population;
        tokens[createdTokenId].recommendedPrice = recommendedPrice;

        totalSupply = totalSupply.add(1);
        emit Transfer(address(0), owner, createdTokenId);
        emit PlanetCreated(createdTokenId, recommendedPrice, age, population, colorValue, galaxyId);
    }

    function finalizeMigration() external
    {
        require(msg.sender == migrationAgent, "Only Migration Agent can call this function");
        migrationAgent = address(0);
        isMigrated = true;
    }
}