pragma solidity ^0.4.24;

import "./interfaces/IPriceHelper.sol";
import './libraries/SafeMath256.sol';

contract PriceHelper is IPriceHelper
{
    using SafeMath256 for uint256;

    uint256 private nonce;
    uint256 private constant UINT16_MAX = ~uint16(0);
    uint256 private constant UINT48_MAX = ~uint48(0);
    uint256 private constant UINT64_MAX = ~uint64(0);

    constructor () public
    {
        nonce = 0;
    }

    function getRandomNumbers(uint256 tokenId) external returns (uint16, uint48, uint48, uint64) 
    {
        uint16 number1 =  uint16(uint256(keccak256(abi.encodePacked(now, msg.sender, nonce, tokenId))) % UINT16_MAX);
        nonce = nonce.add(1);
        uint48 number2 =  uint48(uint256(keccak256(abi.encodePacked(tokenId, now, msg.sender, nonce))) % UINT48_MAX);
        nonce = nonce.add(1);
        uint48 number3 =  uint48(uint256(keccak256(abi.encodePacked(nonce, tokenId, now, msg.sender))) % UINT48_MAX);
        nonce = nonce.add(1);
        uint64 number4 =  uint64(uint256(keccak256(abi.encodePacked(msg.sender, nonce, tokenId, now))) % UINT64_MAX);
        nonce = nonce.add(1);
        return (number1, number2, number3, number4);
    }

    function getRandomUINT48(uint256 tokenId) external returns (uint48) 
    {
        uint48 number =  uint48(uint256(keccak256(abi.encodePacked(nonce, tokenId, now, msg.sender))) % UINT48_MAX);
        nonce = nonce.add(1);
        return number;
    }

    //estimates the price of token by following rules
    //the bigger age => more interesting collectible => price increases
    //the bigger galaxyId means that planet is far away in a space => more interesting collectible => price increases
    //the bigger population => more interesting collectible => price increases
    //with the current model of price estimation user can get a token that worth of almost 10 ETH, though it will happen extremely rare
    function calculatePlanetPrice(uint48 age, uint64 galaxyId, uint48 population) external pure returns (uint256) 
    {
        uint256 result = age + galaxyId / 2 + uint256(population) * 3;
        return result;
    }
}