pragma solidity ^0.4.24;

import "./Owner.sol";
import "./interfaces/ICryptoPlanets.sol";
import "./interfaces/ICryptoPlanetsNew.sol";

contract MigrationAgent is Ownable 
{
    address public source;
    address public target;

    ICryptoPlanets public sourceContract;
    ICryptoPlanetsNew public targetContract;

    constructor(address _source) public 
    {
        source = _source;
    }

    function setTargetToken(address _target) public onlyOwner 
    {
        require(target == address(0), "Target contract was already set");
        target = _target;
        sourceContract = ICryptoPlanets(source);
        targetContract = ICryptoPlanetsNew(target);
    }

    function safetyInvariantCheck() private view
    {
        require(target != address(0), "Tokens are out");
    }

    function migrateToken(address owner, uint48 age, uint64 galaxyId, uint48 population, uint16 colorValue, uint256 recommendedPrice) public
    {
        require(msg.sender == source, "Only source contract can call this method");
        targetContract.receiveMigratedData(owner, age, galaxyId, population, colorValue, recommendedPrice);
    }

    function finalizeMigration() public onlyOwner 
    {
        safetyInvariantCheck();
        sourceContract.finalizeMigration();
        targetContract.finalizeMigration();
        source = address(0);
        target = address(0);
    }
}