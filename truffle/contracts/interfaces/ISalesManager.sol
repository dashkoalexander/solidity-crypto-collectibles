pragma solidity ^0.4.24;

contract ISalesManager 
{
    event PlanetSold(address indexed buyer, uint256 indexed tokenId);
    event BuyRequestCreated(address indexed buyer, uint256 indexed tokenId, uint256 amount);
    event BuyRequestCancelled(address indexed buyer, uint256 indexed tokenId, uint256 amount);
    event BuyRequestApproved(address indexed buyer, uint256 indexed tokenId, uint256 indexed amount, address seller);
    
    function setPlanetsContract(address planetsContractAddress) public;

    function buyPlanet() payable public returns(uint256);
    //gives a chance to increase a recommended price for token by changing it's inner state 
    function ruinPlanet(uint256 tokenId) payable public;
    
    //methods that allow trading tokens (buyer can ignore the recommended price and set the desirable one) 
    function buyPlanetRequest(uint256 tokenId) payable public;
    function approveBuyPlanetRequest(uint256 tokenId, address buyerAddress) public;

    function setTokenPrice(uint256 newTokenPrice) external;
    function setRuinTokenPrice(uint256 newRuinTokenPrice) external;
    function getTokenPrice() external view returns(uint256);
    function getRuinTokenPrice() external view returns(uint256);

    //called by owner to withdraw all the available profit
    function sendAllProfit(address recepient) external;  
    function getBalance() external view returns(uint256);  
}