pragma solidity ^0.4.24;

contract IPriceHelper
{
    function getRandomNumbers(uint256 tokenId) external returns (uint16, uint48, uint48, uint64);
    function getRandomUINT48(uint256 tokenId) external returns (uint48);
    function calculatePlanetPrice(uint48 age, uint64 galaxyId, uint48 population) external pure returns (uint256);
}