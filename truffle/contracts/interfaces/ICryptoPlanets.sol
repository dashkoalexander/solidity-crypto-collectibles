pragma solidity ^0.4.24;

contract ICryptoPlanets
{
    event PlanetCreated(uint256 indexed tokenId, uint256 price, uint48 age, uint48 population, uint16 color, uint64 galaxyId);
    event PlanetRuined(uint256 indexed tokenId, uint256 newPrice, uint48 newAge, uint48 newPopulation);

    //gives a chance to increase a recommended price for token by changing it's inner state 
    function ruinPlanet(uint256 tokenId) external;

    function setSalesManagerAddress(address _salesManagerAddress) public;

    //ERC721 implementation
    event Transfer(address indexed from, address indexed to, uint256 indexed tokenId);
    event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId);
    
    function balanceOf(address owner) external view returns (uint256);
    function ownerOf(uint256 tokenId) public view returns (address);
    function transferFrom(address from, address to, uint256 tokenId) external;
    function mint(address to) external returns(uint256);

    function approve(address approved, uint256 tokenId) external;
    function getApproved(uint256 tokenId) public view returns (address);
    
    //migration implementation
    event Migrate(address indexed owner, uint256 indexed tokenId);

    function setMigrationAgent(address agent) external;
    function migrate(uint256 tokenId) external;
    function finalizeMigration() external;
}