pragma solidity ^0.4.24;

contract IMigrationAgent
{
    function finalizeMigration() external;
    function migrateToken(address owner, uint48 age, uint64 galaxyId, uint48 population, uint16 colorValue, uint256 recommendedPrice) public;
}