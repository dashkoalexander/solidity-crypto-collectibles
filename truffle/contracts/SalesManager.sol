pragma solidity ^0.4.24;

import "./Owner.sol";
import "./interfaces/ISalesManager.sol";
import "./interfaces/ICryptoPlanets.sol";
import './libraries/SafeMath256.sol';

contract SalesManager is ISalesManager, Ownable
{
    using SafeMath256 for uint256;

    uint256 private tokenPrice;
    uint256 private ruinTokenPrice;
    ICryptoPlanets private planetsContract;

    mapping (address => mapping (uint256 => uint256)) private buyRequests;

    event PlanetSold(address indexed buyer, uint256 indexed tokenId);
    event BuyRequestCreated(address indexed buyer, uint256 indexed tokenId, uint256 amount);
    event BuyRequestCancelled(address indexed buyer, uint256 indexed tokenId, uint256 amount);
    event BuyRequestApproved(address indexed buyer, uint256 indexed tokenId, uint256 indexed amount, address seller);

    constructor (address planetsContractAddress) public
    {
        tokenPrice = 1 ether;
        ruinTokenPrice = 0.1 ether;
        setPlanetsContract(planetsContractAddress);
    }

    function setPlanetsContract(address planetsContractAddress) public onlyOwner
    {
        planetsContract = ICryptoPlanets(planetsContractAddress);
    }

    function() payable external
    {
        revert("This contract doesn't accept ETH directly..");
    }

    //main payable fallback function, mints the token if succeeded 
    function buyPlanet() payable public returns(uint256)
    {
        require(tokenPrice > 0, "Planets can't be created now, try later.");

        uint256 amount = msg.value;
        require(amount >= tokenPrice, "Not enough ETH for buying a planet.");
        
        //returns the unused ETH to sender
        uint256 returnAmount = amount.sub(tokenPrice);
        if(returnAmount > 0)
        {
            msg.sender.transfer(returnAmount);
        }
        
        uint256 tokenId = planetsContract.mint(msg.sender);
        emit PlanetSold(msg.sender, tokenId);
        return tokenId;
    }

    //second payable function, gives a chance to increase a recommended price for token by changing it's inner state 
    function ruinPlanet(uint256 tokenId) payable public
    {
        require(ruinTokenPrice > 0, "Planets can't be created now, try later.");
        require(planetsContract.ownerOf(tokenId) == msg.sender, "You are not the owner of the specified planet.");

        uint256 amount = msg.value;
        require(amount >= ruinTokenPrice, "Not enough ETH for ruining the planet.");
        
        //returns the unused ETH to sender
        uint256 returnAmount = amount.sub(ruinTokenPrice);
        if(returnAmount > 0)
        {
            msg.sender.transfer(returnAmount);
        }

        planetsContract.ruinPlanet(tokenId);
    }

    //auction-like method - buyer propose his own price for the token
    function buyPlanetRequest(uint256 tokenId) payable public
    {
        require(planetsContract.ownerOf(tokenId) != address(0), "This token doesn't exists.");
        require(planetsContract.ownerOf(tokenId) != msg.sender, "You can't buy your planet again.");

        uint256 amount = msg.value;
        require(amount != 0, "You need to spend some ETH.");

        buyRequests[msg.sender][tokenId] = amount;
        emit BuyRequestCreated(msg.sender, tokenId, amount);
    }

    //buyer is able to cancel the request before it was accepted by token owner
    function cancelBuyPlanetRequest(uint256 tokenId) public
    {
        uint256 amount = buyRequests[msg.sender][tokenId];
        require(amount > 0, "You haven't put funds earlier for this planet or seller has accepted your request.");

        buyRequests[msg.sender][tokenId] = 0;
        msg.sender.transfer(amount);
        emit BuyRequestCancelled(msg.sender, tokenId, amount);
    }

    //token owner can accept buyer's request
    function approveBuyPlanetRequest(uint256 tokenId, address buyerAddress) public
    {
        require(planetsContract.ownerOf(tokenId) == msg.sender, "You can't approve buy request for another user's planet.");
        
        uint256 amount = buyRequests[buyerAddress][tokenId];
        require(amount != 0, "You can't approve buy request for user who haven't requested it.");

        msg.sender.transfer(amount);
        delete buyRequests[buyerAddress][tokenId];
        planetsContract.transferFrom(msg.sender, buyerAddress, tokenId);
        emit BuyRequestApproved(buyerAddress, tokenId, amount, msg.sender);
    }

    function setTokenPrice(uint256 newTokenPrice) external onlyOwner
    {
        tokenPrice = newTokenPrice;
    }

    function setRuinTokenPrice(uint256 newRuinTokenPrice) external onlyOwner
    {
        ruinTokenPrice = newRuinTokenPrice;
    }

    function getTokenPrice() external view returns(uint256)
    {
        return tokenPrice;
    }

    function getRuinTokenPrice() external view returns(uint256)
    {
        return ruinTokenPrice;
    }

    function sendAllProfit(address recepient) external onlyOwner
    {
        recepient.transfer(address(this).balance);
    }

    function getBalance() external view onlyOwner returns(uint256)
    {
        return address(this).balance;
    }
}