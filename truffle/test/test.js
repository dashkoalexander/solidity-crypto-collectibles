const CryptoPlanets = artifacts.require("CryptoPlanets");
const SalesManager = artifacts.require("SalesManager");
const PriceHelper = artifacts.require("PriceHelper");
const assert = require("chai").assert;
const truffleAssert = require('truffle-assertions');

contract('SalesManager + CryptoPlanets', (accounts) => {
    let salesManager;
    let cryptoPlanets;
    let priceHelper;
    let token1Id;
    let token2Id;
    const account1 = accounts[0];
    const account2 = accounts[1];

    before(async () => {
        priceHelper = await PriceHelper.new({from: account1});
        cryptoPlanets = await CryptoPlanets.new(priceHelper.address, {from: account1});
        salesManager = await SalesManager.new(cryptoPlanets.address, {from: account1});
    });

    it("should not be able to set sales manager if not owner", async () => {
        await truffleAssert.reverts(
            cryptoPlanets.setSalesManagerAddress(salesManager.address, {from: account2})
        );
    });

    it("should be able to set sales manager if owner", async () => {
        await cryptoPlanets.setSalesManagerAddress(salesManager.address, {from: account1})
    });

    it("should not be able to send ETH to fallback function", async () => {
        var amount = 100000000000000000; //0.1 ETH
        await truffleAssert.reverts(
            salesManager.sendTransaction({from: account1, value: amount})
        );
    });

    it("should not be able to buying a planet with less ETH than price stated", async () => {
        var amount = 900000000000000000; //0.9 ETH
        await truffleAssert.reverts(
            salesManager.buyPlanet({from: account1, value: amount})
        );
    });

    it("should not be able to ruin the planet that is not created yet", async () => {
        var amount = 100000000000000000; //0.1 ETH
        await truffleAssert.reverts(
            salesManager.ruinPlanet(0, {from: account1, value: amount})
        );
    });

    it("should be able to buy the planet with some ETH returned back", async () => {
        var amount = 1500000000000000000; //1.5 ETH of 1 ETH needed, so 0.5 ETH should be returned back

        let tx = await salesManager.buyPlanet({from: account1, value: amount});
        truffleAssert.eventEmitted(tx, 'PlanetSold', (ev) => {
            token1Id = Number(ev.tokenId);
            return ev.buyer === account1;
        });

        let planet1Owner = await cryptoPlanets.ownerOf(token1Id, {from: account1});
        assert.equal(planet1Owner, account1, "Planet was assigned to the wrong address");

        let contractBalance = await salesManager.getBalance({from: account1});
        let balance = contractBalance.toNumber();

        //this indicates that balance is correct and 0.5 ETH were returned from SalesManager back to owners
        assert.equal(balance, 1000000000000000000, "Contract got more ETH than price stated");
    });

    it("should be able to buy the planet with the exact amount of ETH and from another account", async () => {
        var amount = 1000000000000000000; //1 ETH of 1 ETH needed, no ETH should be returned back

        let tx = await salesManager.buyPlanet({from: account2, value: amount});
        truffleAssert.eventEmitted(tx, 'PlanetSold', (ev) => {
            token2Id = Number(ev.tokenId);
            return ev.buyer === account2;
        });

        let planet2Owner = await cryptoPlanets.ownerOf(token2Id, {from: account2});
        assert.equal(planet2Owner, account2, "Planet was assigned to the wrong address");

        let contractBalance = await salesManager.getBalance({from: account1});
        let balance = contractBalance.toNumber();

        //this indicates that balance is correct
        assert.equal(balance, 2000000000000000000, "Contract got more ETH than price stated");
    });

    //testing ruining the created planet with less ETH than required for such operation
    it("should not be able to ruin the planet with less ETH than price stated", async () => {
        var amount = 10000000000000000; //0.01 ETH
        await truffleAssert.reverts(
            salesManager.ruinPlanet(0, {from: account1, value: amount})
        );
    });

    it("should not be able to withdraw the balance - not an owner", async () => {
        await truffleAssert.reverts(
            salesManager.sendAllProfit(account2, {from: account2})
        );
    });

    it("should be able to withdraw the balance - owner", async () => {
        await salesManager.sendAllProfit(account1, {from: account1});

        let contractBalance = await salesManager.getBalance({from: account1});
        let balance = contractBalance.toNumber();

        //this indicates that balance is correct and all ETH were sent out from SalesManager
        assert.equal(balance, 0, "Contract still has some ETH");
    });

    //testing ruining the created planet with even more ETH than required for such operation
    it("should be able to ruin the planet with some ETH returned back", async () => {
        var amount = 200000000000000000; //0.2 ETH of 0.1 ETH needed, so 0.1 ETH should be returned back
        await salesManager.ruinPlanet(0, {from: account1, value: amount});

        let contractBalance = await salesManager.getBalance({from: account1});
        let balance = contractBalance.toNumber();

        //this indicates that balance is correct and 0.1 ETH were returned from SalesManager
        assert.equal(balance, 100000000000000000, "Contract got more ETH than price stated");
    });
})