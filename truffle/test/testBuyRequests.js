const CryptoPlanets = artifacts.require("CryptoPlanets");
const SalesManager = artifacts.require("SalesManager");
const PriceHelper = artifacts.require("PriceHelper");
const assert = require("chai").assert;
const truffleAssert = require('truffle-assertions');

contract('Buy Requests', (accounts) => {
    let salesManager;
    let cryptoPlanets;
    let priceHelper;
    let tokenId;
    const account1 = accounts[0];
    const account2 = accounts[1];
    const account3 = accounts[2];

    before(async () => {
        priceHelper = await PriceHelper.new({from: account1});
        cryptoPlanets = await CryptoPlanets.new(priceHelper.address, {from: account1});
        salesManager = await SalesManager.new(cryptoPlanets.address, {from: account1});
        await cryptoPlanets.setSalesManagerAddress(salesManager.address, {from: account1});
    });

    it("should be able to buy the planet", async () => {
        var amount = 1000000000000000000; //1 ETH

        let tx = await salesManager.buyPlanet({from: account1, value: amount});
        truffleAssert.eventEmitted(tx, 'PlanetSold', (ev) => {
            tokenId = Number(ev.tokenId);
            return ev.buyer === account1;
        });

        let planetOwner = await cryptoPlanets.ownerOf(tokenId, {from: account1});
        assert.equal(planetOwner, account1, "Planet was assigned to the wrong address");
    });

    it("should not be able to cancel buy request that doesn't exist", async () => {
        await truffleAssert.reverts(
            salesManager.cancelBuyPlanetRequest(tokenId, {from: account2})
        );
    });

    it("should not be able to buy the planet that doesn't exist", async () => {
        var amount = 1000000000000000000; //1 ETH
        await truffleAssert.reverts(
            salesManager.buyPlanetRequest((tokenId + 1), {from: account2, value: amount})
        );
    });
    
    it("should not be able to buy own planet again", async () => {
        var amount = 1000000000000000000; //1 ETH
        await truffleAssert.reverts(
            salesManager.buyPlanetRequest(tokenId, {from: account1, value: amount})
        );
    });
    
    it("should not be able to buy the planet with no ETH spent", async () => {
        var amount = 0;
        await truffleAssert.reverts(
            salesManager.buyPlanetRequest(tokenId, {from: account2, value: amount})
        );
    });
    
    it("should be able to create a buy request", async () => {
        var amount = 1000000000000000000; //1 ETH
        await salesManager.buyPlanetRequest(tokenId, {from: account2, value: amount})
    });

    it("should be able to cancel own buy request", async () => {
        await salesManager.cancelBuyPlanetRequest(tokenId, {from: account2});
    });

    it("should be able to create a buy request again", async () => {
        var amount = 1000000000000000000; //1 ETH
        await salesManager.buyPlanetRequest(tokenId, {from: account2, value: amount})
    });

    it("should not be able to approve buy request for another user's planet", async () => {
        await truffleAssert.reverts(
            salesManager.approveBuyPlanetRequest((tokenId + 1), account2, {from: account1})
        );
    });
    
    it("should not be able to approve buy request for user who haven't requested it", async () => {
        await truffleAssert.reverts(
            salesManager.approveBuyPlanetRequest(tokenId, account3, {from: account1})
        );
    });

    it("should be able to approve buy request for own planet", async () => {
        await salesManager.approveBuyPlanetRequest(tokenId, account2, {from: account1})
        
        let planetOwner = await cryptoPlanets.ownerOf(tokenId, {from: account2});
        assert.equal(planetOwner, account2, "Planet was assigned to the wrong address");
    });
})