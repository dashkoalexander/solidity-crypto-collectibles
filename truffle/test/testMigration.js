const CryptoPlanets = artifacts.require("CryptoPlanets");
const CryptoPlanetsNew = artifacts.require("CryptoPlanetsNew");
const SalesManager = artifacts.require("SalesManager");
const PriceHelper = artifacts.require("PriceHelper");
const MigrationAgent = artifacts.require("MigrationAgent");
const assert = require("chai").assert;
const truffleAssert = require('truffle-assertions');

contract('Migration', (accounts) => {
    let salesManager;
    let cryptoPlanets;
    let cryptoPlanetsNew;
    let priceHelper;
    let migrationAgent;
    let token1Id;
    let token2Id;
    const account1 = accounts[0];
    const account2 = accounts[1];

    before(async () => {
        priceHelper = await PriceHelper.new({from: account1});
        cryptoPlanets = await CryptoPlanets.new(priceHelper.address, {from: account1});
        salesManager = await SalesManager.new(cryptoPlanets.address, {from: account1});
        await cryptoPlanets.setSalesManagerAddress(salesManager.address, {from: account1});
    });

    it("should be able to buy the planet for account 1", async () => {
        var amount = 1000000000000000000; //1 ETH

        let tx = await salesManager.buyPlanet({from: account1, value: amount});
        truffleAssert.eventEmitted(tx, 'PlanetSold', (ev) => {
            token1Id = Number(ev.tokenId);
            return ev.buyer === account1;
        });

        let planet1Owner = await cryptoPlanets.ownerOf(token1Id, {from: account1});
        assert.equal(planet1Owner, account1, "Planet was assigned to the wrong address");
    });

    it("should be able to buy the planet for account 2", async () => {
        var amount = 1000000000000000000; //1 ETH

        let tx = await salesManager.buyPlanet({from: account2, value: amount});
        truffleAssert.eventEmitted(tx, 'PlanetSold', (ev) => {
            token2Id = Number(ev.tokenId);
            return ev.buyer === account2;
        });

        let planet2Owner = await cryptoPlanets.ownerOf(token2Id, {from: account2});
        assert.equal(planet2Owner, account2, "Planet was assigned to the wrong address");
    });

    //MIGRATION TESTS
    it("should not be able to migrate token before migration set up", async () => {
        await truffleAssert.reverts(
            cryptoPlanets.migrate(token1Id, {from: account1})
        );
    });

    it("should be able to deploy migration contract and target contract", async () => {
        cryptoPlanetsNew = await CryptoPlanetsNew.new(priceHelper.address, {from: account1});
        migrationAgent = await MigrationAgent.new(cryptoPlanets.address, {from: account1});
        await cryptoPlanetsNew.setSalesManagerAddress(salesManager.address, {from: account1});
    });

    it("should not be able to set migration agent in source contract if not owner", async () => {
        await truffleAssert.reverts(
            cryptoPlanets.setMigrationAgent(migrationAgent.address, {from: account2})
        );
    });

    it("should be able to set migration agent in source contract if owner", async () => {
        await cryptoPlanets.setMigrationAgent(migrationAgent.address, {from: account1})
    });

    it("should not be able to set migration agent in target contract if not owner", async () => {
        await truffleAssert.reverts(
            cryptoPlanetsNew.setMigrationAgent(migrationAgent.address, {from: account2})
        );
    });

    it("should be able to set migration agent in target contract if owner", async () => {
        await cryptoPlanetsNew.setMigrationAgent(migrationAgent.address, {from: account1})
    });

    it("should not be able to set target in migration contract if not owner", async () => {
        await truffleAssert.reverts(
            migrationAgent.setTargetToken(cryptoPlanetsNew.address, {from: account2})
        );
    });

    it("should not be able to finalize migration before target contract was set", async () => {
        await truffleAssert.reverts(
            migrationAgent.finalizeMigration({from: account1})
        );
    });

    it("should be able to set target in migration contract if owner", async () => {
        await migrationAgent.setTargetToken(cryptoPlanetsNew.address, {from: account1})
    });

    it("should not be able to finalize migration if not owner", async () => {
        await truffleAssert.reverts(
            migrationAgent.finalizeMigration({from: account2})
        );
    });

    it("should not be able to set target in migration contract twice", async () => {
        await truffleAssert.reverts(
            migrationAgent.setTargetToken(cryptoPlanetsNew.address, {from: account1})
        );
    });

    it("should not be able to migrate token not existing token", async () => {
        await truffleAssert.reverts(
            cryptoPlanets.migrate((token1Id + 1), {from: account1})
        );
    });

    it("should not be able to migrate token if not it's owner", async () => {
        await truffleAssert.reverts(
            cryptoPlanets.migrate(token1Id, {from: account2})
        );
    });

    it("should be able to migrate token", async () => {
        await cryptoPlanets.migrate(token1Id, {from: account1})
    });

    it("should not be able to migrate token twice", async () => {
        await truffleAssert.reverts(
            cryptoPlanets.migrate(token1Id, {from: account1})
        );
    });

    it("should be able to finalize migration if owner", async () => {
        await migrationAgent.finalizeMigration({from: account1});
    });
    
    it("should not be able to migrate token after migration is finalized", async () => {
        await truffleAssert.reverts(
            cryptoPlanets.migrate(token2Id, {from: account2})
        );
    });
})